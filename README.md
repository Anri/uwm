# UWM

Récupère les notes depuis le site de Paris 8

[![dependency status](https://deps.rs/repo/gitea/git.kennel.ml/Anri/uportalWrapperMarks/status.svg)](https://deps.rs/repo/gitea/git.kennel.ml/Anri/uportalWrapperMarks)

## Usage

### En ligne de commande

```
$ uwm username
```

### Avec un fichier de configuration `config.toml`

```toml
username = "username" # Exemple pour Alice Dupont : adupont
password = "password" # Facultatif
```

_Attention : le mot de passe est stocké en clair_

-   Linux : `~/.config/uwm/config.toml`
-   Windows : `%APPDATA%\anri\uwm\config.toml`
-   MacOS : `~/Library/Application Support/com.anri.uwm/config.toml` <!-- i guess? -->

---

Anciennement [uPortalWrapperMarks](https://git.kennel.ml/Anri/uportalWrapperMarks/src/branch/python).
