use clap::Parser;
use directories::ProjectDirs;
use serde::Deserialize;

mod marks;
mod utils;

#[derive(Deserialize)]
struct Config {
    username: String,
    password: Option<String>,
}

#[derive(Parser)]
#[clap(version, about, long_about = None)]
struct Args {
    /// Your username
    #[clap(value_parser)]
    username: Option<String>,
}

#[tokio::main]
async fn main() {
    let args = Args::parse();

    if let Some(proj_dirs) = ProjectDirs::from("com", "anri", "uwm") {
        let config_dir = proj_dirs.config_dir();

        let file_path = config_dir.join("config.toml");

        let config_file = std::fs::read_to_string(&file_path);

        let mut config = match config_file {
            Ok(file) => match toml::from_str(&file) {
                Ok(stored_config) => stored_config,
                Err(file_error) => {
                    // Error in config file
                    if args.username.is_none()
                        && file_error.to_string().contains("missing field `username`")
                    {
                        // Show error message if needed
                        println!("Error in config file: {}", file_error);
                    }
                    Config {
                        username: if args.username.is_some() {
                            args.username.unwrap()
                        } else {
                            utils::ask_username()
                        },
                        password: Some(utils::ask_password()),
                    }
                }
            },
            Err(_) => Config {
                // No config file, ask for creds
                username: if args.username.is_some() {
                    args.username.unwrap()
                } else {
                    // Only ask if nothing has been given in args
                    utils::ask_username()
                },
                password: Some(utils::ask_password()),
            },
        };

        if config.password.is_none() {
            config.password = Some(utils::ask_password());
        }

        let user_agent = format!("uwm/{}", env!("CARGO_PKG_VERSION"));

        marks::get_marks(config.username, config.password.unwrap(), &user_agent).await;
    }
}
