use scraper::{Html, Selector};

/// Retrieves marks for a user
pub async fn get_marks(username: String, password: String, user_agent: &str) {
    // Login
    let client = login(username, password, user_agent).await.unwrap();

    // Marks
    load_marks(&client).await.unwrap();

    println!("Notes *récupérés* !")
}

/// Login to eP8
async fn login(
    username: String,
    password: String,
    user_agent: &str,
) -> Result<reqwest::Client, Box<dyn std::error::Error>> {
    let login_url = "https://cas.univ-paris8.fr/cas/login";

    let client = reqwest::Client::builder()
        .cookie_store(true)
        .user_agent(user_agent)
        .build()?;

    let login_page = Html::parse_document(&client.get(login_url).send().await?.text().await?);

    // LT
    let sel_lt = Selector::parse(r#"input[name="lt"]"#).unwrap();
    let lt = login_page
        .select(&sel_lt)
        .next()
        .unwrap()
        .value()
        .attr("value")
        .unwrap();

    // Execution
    let sel_execution = Selector::parse(r#"input[name="execution"]"#).unwrap();
    let execution = login_page
        .select(&sel_execution)
        .next()
        .unwrap()
        .value()
        .attr("value")
        .unwrap();

    let params = [
        ("username", username.as_str()),
        ("password", password.as_str()),
        ("_eventId", "submit"),
        ("submit", "SE CONNECTER"),
        ("lt", lt),
        ("execution", execution),
    ];

    // Login
    let sel_confirmation = Selector::parse("h2").unwrap();
    if Html::parse_document(
        &client
            .post(login_url)
            .form(&params)
            .send()
            .await?
            .text()
            .await?,
    )
    .select(&sel_confirmation)
    .next()
    .unwrap()
    .inner_html()
    .contains("Connexion réussie")
    {
        println!("Connexion réussie...");
        Ok(client)
    } else {
        panic!("Connexion échouée : Mauvais identifiants")
    }
}

/// Load marks from scolarite-etudiant
async fn load_marks(client: &reqwest::Client) -> Result<Html, Box<dyn std::error::Error>> {
    let html = client
        .get("https://scolarite-etudiant.univ-paris8.fr/mondossierweb/#!notesView")
        .send()
        .await?;

    let document = Html::parse_document(&html.text().await?);

    Ok(document)
}
