use std::io::{self, Write};

/// Ask user for username
pub fn ask_username() -> std::string::String {
    let mut user_input = String::new();
    let stdin = io::stdin();

    print!("Username: ");
    io::stdout().flush().unwrap();
    stdin.read_line(&mut user_input).unwrap();

    user_input.trim_end().to_string()
}

/// Ask user for password
pub fn ask_password() -> std::string::String {
    print!("Password: ");
    io::stdout().flush().unwrap();
    rpassword::read_password().unwrap()
}

#[allow(dead_code)]
/// Write a document to a file
///
/// `html_data` may be created with something like that:
/// ```
/// Html::parse_document(&client.get("URL").send().await?.text().await?).html()
/// ```
pub fn write_html(html_data: String) {
    let mut f = std::fs::File::create("./target/temp.html").unwrap();
    write!(&mut f, "{}", html_data).unwrap();
}
